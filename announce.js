var http = require('http');
var redis = require('redis');
var redisHost = process.env.REDIS_HOST;
var redisPass = process.env.REDIS_PASSWORD;
var rclient = redis.createClient(parseInt(redisHost.split(':')[1], 10), redisHost.split(':')[0], {
  auth_pass: redisPass
});

// grab the IP to advertise based on our environment
if (process.env.ENVIRONMENT !== undefined && (process.env.ENVIRONMENT === 'staging' || process.env.ENVIRONMENT === 'production')) {
  // get the private IP of the DO droplet we are hosted on (so we can tell service discovery where to connect)
  http.request({ host: '169.254.169.254', path: '/metadata/v1/interfaces/private/0/ipv4/address' }, function (res) {
    if (res.statusCode !== 200) {
      process.exit(1);
    }

    var data = '';

    res.on('data', function (chunk) {
      data += chunk;
    });

    res.on('end', function () {
      startHealthChecks(data);
    });
  }).end();
} else {
  // get the IP of the docker container we're running in (as we aren't running in the DO cloud)
  var exec = require('child_process').exec;

  exec("awk 'NR==7 {print $1}' /etc/hosts", function (error, stdout, stderr) {
    if (error) {
      process.exit(1);
    } else {
      startHealthChecks(stdout.trim());
    }
  });
}

function startHealthChecks (privateIp) {
  var keyVal = privateIp + ':' + process.env.HOST_PORT;
  var serviceName = process.env.HOST_KEY;

  function checkSite (url, port, cb) {
    var req = http.request({host: url, port: port}, function (res) {
      if (res.statusCode === 200) {
        cb(true);
      } else {
        cb(false);
      }
    });

    req.on('error', function (e) {
      cb(false);
    });

    req.on('socket', function (socket) {
      socket.setTimeout(2000);
      socket.on('timeout', function () {
        req.abort();
      });
    });

    req.end();
  }

  setInterval(function () {
    checkSite('localhost', 3000, function (success) {
      if (success) {
        rclient.setex('/services/' + serviceName + '/' + keyVal, 30, keyVal);
      } else {
        rclient.del('/services/' + serviceName + '/' + keyVal);
      }
    });
  }, 5 * 1000);
}
